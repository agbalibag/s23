 //Insert a single room

    db.users.insertOne({
    	"name": "single",
    	"accomodates": 2,
    	"price": 1000,
    	"description": "A simple room with all the basic necessities",
    	"rooms_available": 10,
    	"isAvailable": false   
    });



//Insert multiple rooms

    db.users.insertMany([
    	{
    	"name": "double",
    	"accomodates": 3,
    	"price": 2000,
    	"description": "A room suitable for two people who are on vacation",
    	"rooms_available": 5,
    	"isAvailable": false  
    	},
    	{
    	"name": "queen",
    	"accomodates": 4,
    	"price": 4000,
    	"description": "A room suitable for families who are on vacation",
    	"rooms_available": 6,
    	"isAvailable": false  
    	}
    ])



 //Use the find method to search for a room

    db.users.find({"name": "double"})


//Use the updateOne method to update the queen room

    db.users.updateOne(
    	{"_id":ObjectId("61d78a0c71540dd84d3ee719")
    	},
    	{
    	$set: {
    		"rooms_available": 0
    	  }
    	}
    );



 //Use the deleteMany method to delete rooms

    db.users.deleteMany({ "rooms_available": 0 });